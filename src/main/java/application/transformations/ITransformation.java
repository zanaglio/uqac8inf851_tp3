package application.transformations;

import java.awt.image.BufferedImage;

/** Interface for transformations
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public interface ITransformation {

	BufferedImage transform(BufferedImage image);

	String getIdentification();

	boolean isUnique();

}
