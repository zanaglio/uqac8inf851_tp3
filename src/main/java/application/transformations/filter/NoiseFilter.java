package application.transformations.filter;


import application.transformations.ITransformation;

/** Noise Filter
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class NoiseFilter extends AbstractFilter {

	private static final ITransformation instance = new NoiseFilter();
	
	private NoiseFilter(){
		super(new float[][]{ { 0.0f, -1.0f, 0.0f, -1.0f, 5.0f, -1.0f, 0.0f, -1.0f, 0.0f }}, 3);
	}

	@Override
	public String getIdentification() {
		return "Noise Filter";
	}

	public static ITransformation getInstance() {
		return instance;
}
}