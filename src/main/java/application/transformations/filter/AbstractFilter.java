package application.transformations.filter;

import application.transformations.ITransformation;

import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;

/** Abstract class to describe filters
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public abstract class AbstractFilter implements ITransformation {

	private final float[][] masks;
	private final int dimension;

	AbstractFilter(float[][] masks, int dimension){
		this.masks = masks;
		this.dimension=dimension;		
	}


	/**
	 * Apply all the filter to the buffered image by calling <code>appliquerFilter</code>
	 * @param  image  image we want to apply the filter in the array masks
	 */
	@Override
	public BufferedImage transform(BufferedImage image){
		BufferedImage res=image;
		for(float[] m : masks){
			res= applyFilter(res, m);
		}
		return res;
		
	}

	/**
	 * Create a new image by applying the filter
	 * @param  image  image we want to apply the filter
	 * @param  filter array of float which defines the filter
	 *
	 * To modify the image, we create a convolution masks, initialized with a
	 * kernel (defined by the filter variable). Then we apply the convolution masks
	 */
	private BufferedImage applyFilter(BufferedImage image, float[] filter){
		Kernel kernel = new Kernel(dimension, dimension, filter);
		ConvolveOp convo = new ConvolveOp(kernel);
        return convo.filter(image, null);

    }



	/**
	 * Return a boolean which says if the filter can be applied multiple times
	 */
	@Override
	public boolean isUnique() {
		return false;
	}

}
