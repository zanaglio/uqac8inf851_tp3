package application.transformations.filter;


import application.transformations.ITransformation;

/** Average Filter
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class AverageFilter extends AbstractFilter {

	private static final ITransformation instance = new AverageFilter();

	private AverageFilter(){
		super(new float[][]{ { 1/9f, 1/9f, 1/9f, 1/9f, 1/9f, 1/9f, 1/9f, 1/9f, 1/9f}}, 3);
	}

	@Override
	public String getIdentification() {
		return "Average Filter";
	}

	public static ITransformation getInstance() {
		return instance;
	}
}