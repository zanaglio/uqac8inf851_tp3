package application.transformations.filter;


import application.transformations.ITransformation;

/** Prewitt Filter
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class PrewittFilter extends AbstractFilter {

	private static final ITransformation instance = new PrewittFilter();
	
	private PrewittFilter(){
		super(new float[][]{new float[]{-1f, 0f, 1f, -1f, 0f, 1f, -1f, 0f, 1f}, new float[]{1f, 1f, 1f, 0f, 0f, 0f, -1f, -1f, -1f}}, 3);
	}

	@Override
	public String getIdentification() {
		return "Prewitt Filter";
	}

	public static ITransformation getInstance() {
		return instance;
}
}