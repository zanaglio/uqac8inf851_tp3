package application.transformations.filter;


import application.transformations.ITransformation;

/** Gaussian Filter
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class GaussianFilter extends AbstractFilter {

	private static final ITransformation instance = new GaussianFilter();

	private GaussianFilter(){
		super(new float[][]{ { 4/1344f, 18/1344f, 19/1344f, 18/1344f, 4/1344f, 18/1344f, 80/1344f, 132/1344f, 80/1344f, 18/1344f, 29/1344f, 132/1344f, 218/1344f, 132/1344f, 29/1344f, 18/1344f, 80/1344f, 132/1344f, 80/1344f, 18/1344f, 4/1344f, 18/1344f, 29/1344f, 18/1344f, 4/1344f}}, 5);
	}

	@Override
	public String getIdentification() {
		return "Gaussian Filter";
	}

	public static ITransformation getInstance() {
		return instance;
	}
}