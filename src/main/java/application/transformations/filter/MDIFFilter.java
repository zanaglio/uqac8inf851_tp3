package application.transformations.filter;


import application.transformations.ITransformation;

/** MDIFF Filter
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class MDIFFilter extends AbstractFilter {

	private static final ITransformation instance = new MDIFFilter();
	
	private MDIFFilter(){
		super(new float[][]{new float[]{0f, 1f, 0f, -1f, 0f, 1f, 2f, 0f, -2f, -1f, 1f, 3f, 0f, -3f, -1f, 1f, 2f, 0f, -2f, -1f, 0f, 1f, 0f, -1f, 0f}, new float[]{0f, 1f, 1f, 1f, 0f, 1f, 2f, 3f, 2f, 1f, 0f, 0f, 0f, 0f, 0f, -1f, -2f, -3f, -2f, -1f, 0f, -1f, -1f, -1f, 0f}}, 5);
	}

	@Override
	public String getIdentification() {
		return "MDIF Filter";
	}

	public static ITransformation getInstance() {
		return instance;
}
}