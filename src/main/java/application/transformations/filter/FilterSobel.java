package application.transformations.filter;


import application.transformations.ITransformation;

/** Sobel Filter
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class FilterSobel extends AbstractFilter {

	private static final ITransformation instance = new FilterSobel();
	
	private FilterSobel(){
		super(new float[][]{new float[]{1f, 0f, -1f, 2f, 0f, -2f, 1f, 0f, -1f}, new float[]{1f, 2f, 1f, 0f, 0f, 0f, -1f, -2f, -1f}}, 3);
	}

	@Override
	public String getIdentification() {
		return "Sobel Filter";
	}

	public static ITransformation getInstance() {
		return instance;
}
}