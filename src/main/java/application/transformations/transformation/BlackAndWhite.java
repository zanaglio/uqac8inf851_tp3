package application.transformations.transformation;

import application.transformations.ITransformation;

import java.awt.image.BufferedImage;

/** Blacn and white Filter
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class BlackAndWhite extends AbstractTransformation implements ITransformation {

	private static final ITransformation instance = new BlackAndWhite();
	private int m=0;

	private BlackAndWhite(){
		super();
	}
	
	@Override
	public void doTransformation(int i, int j, BufferedImage image,
								 BufferedImage img) {
		int rgb = image.getRGB(i,j);
		if(m==0){
			m=getRGBMoyen(image);
		}
		if(rgb<m){
			img.setRGB(i,j,-16777216);
		}
		else {
			img.setRGB(i,j,-1);
		}
		
	}
	
	private int getRGBMoyen(BufferedImage image){
		int w = image.getWidth();
		int h = image.getHeight();
		int[] rgbs = new int[w*h];
		image.getRGB(0,0,w,h,rgbs,0,w);
		int moyenne=0;
		for(int i=0; i<w*h; i++){
			moyenne+=rgbs[i]/(w*h);
		}
		return moyenne;
	}

	@Override
	public String getIdentification() {
		return "Black and white";
	}

	@Override
	public boolean isUnique() {
		return true;
	}

	/**
	 * @return the instance
	 */
	public static ITransformation getInstance() {
		return instance;
	}

	
	
}
