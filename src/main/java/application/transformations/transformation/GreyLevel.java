package application.transformations.transformation;

import application.transformations.ITransformation;

import java.awt.*;
import java.awt.image.BufferedImage;

/** Regular grey level Filter
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class GreyLevel extends AbstractTransformation implements ITransformation {

	private static final ITransformation instance = new GreyLevel();

	private GreyLevel(){
		super();
	}
	
	@Override
	public void doTransformation(int i, int j, BufferedImage image,
								 BufferedImage img) {
		Color c = new Color(image.getRGB(i, j));
		int nb=(c.getRed()+c.getBlue()+c.getGreen())/3;
		img.setRGB(i,j,new Color(nb, nb, nb).getRGB());

	}


	public static ITransformation getInstance() {
		return instance;
	}

	@Override
	public String getIdentification() {
		return "Grey level";
	}

	@Override
	public boolean isUnique() {
		return true;
	}



}
