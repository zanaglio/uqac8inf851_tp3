package application.transformations.transformation;

import application.transformations.ITransformation;

import java.awt.*;
import java.awt.image.BufferedImage;

/** Reverse colors Filter
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class ReverseColour extends AbstractTransformation implements ITransformation {

    private static final ITransformation instance = new ReverseColour();

    private ReverseColour() {
        super();
    }

    public static ITransformation getInstance() {
        return instance;
    }

    @Override
    public void doTransformation(int i, int j, BufferedImage image,
                                 BufferedImage img) {
        Color c = new Color(image.getRGB(i, j));
        Color newColour = new Color(255 - c.getRed(), 255 - c.getGreen(), 255 - c.getBlue());
        img.setRGB(i, j, newColour.getRGB());
    }

    @Override
    public String getIdentification() {
        return "Inversion couleurs";
    }

    @Override
    public boolean isUnique() {
        return false;
    }

}
