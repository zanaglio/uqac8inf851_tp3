package application.transformations.transformation.balanced;


import application.transformations.ITransformation;

/** Gray level balanced Filter
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class GreyLevelBalanced extends AbstractBalancing implements ITransformation {

	private static final ITransformation instance = new GreyLevelBalanced();
	
	private GreyLevelBalanced(){
		super(0.299f, 0.587f, 0.114f, 0.299f, 0.587f, 0.114f, 0.299f, 0.587f, 0.114f);
	}

	@Override
	public String getIdentification() {
		return "Grey level Balanced";
	}

	@Override
	public boolean isUnique() {
		return true;
	}

	/**
	 * @return the instance
	 */
	public static ITransformation getInstance() {
		return instance;
	}
	
	

}
