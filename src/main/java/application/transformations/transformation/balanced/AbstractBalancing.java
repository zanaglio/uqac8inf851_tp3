package application.transformations.transformation.balanced;

import application.transformations.ITransformation;
import application.transformations.transformation.AbstractTransformation;

import java.awt.*;
import java.awt.image.BufferedImage;

/** Abstract class to describe color balances
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public abstract class AbstractBalancing extends AbstractTransformation implements ITransformation {

	private final float redRed;
	private final float redGreen;
	private final float redBlue;
	private final float greenRed;
	private final float greenGreen;
	private final float greenBlue;
	private final float blueRed;
	private final float blueGreen;
	private final float blueBlue;
	
	AbstractBalancing(float redRed, float redGreen, float redBlue,
					  float greenRed, float greenGreen, float greenBlue, float blueRed,
					  float blueGreen, float blueBlue) {
		this.redRed = redRed;
		this.redGreen = redGreen;
		this.redBlue = redBlue;
		this.greenRed = greenRed;
		this.greenGreen = greenGreen;
		this.greenBlue = greenBlue;
		this.blueRed = blueRed;
		this.blueGreen = blueGreen;
		this.blueBlue = blueBlue;
	}


	/**
	 * Apply a balanced transformation by multiplying the pixels values by a float factor
	 * @param  i absciss of the pixel
	 * @param  j ordinate of the pixel
	 * @param  image source image
	 * @param  res result image
	 */
	@Override
	public void doTransformation(int i, int j, BufferedImage image, BufferedImage res) {

		Color c = new Color(image.getRGB(i, j));
		int outputRed = (int) Math.min(255, ((c.getRed() * redRed) + (c.getGreen() *redGreen) + (c.getBlue() * redBlue)));
		int outputGreen = (int) Math.min(255, ((c.getRed() * greenRed) + (c.getGreen() *greenGreen) + (c.getBlue() *greenBlue)));
		int outputBlue = (int) Math.min(255, ((c.getRed() * blueRed) + (c.getGreen() *blueGreen) + (c.getBlue() * blueBlue)));
		res.setRGB(i,j,new Color(outputRed, outputGreen, outputBlue).getRGB());

	}
}
