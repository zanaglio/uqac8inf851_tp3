package application.transformations.transformation;

import application.transformations.ITransformation;

import java.awt.*;
import java.awt.image.BufferedImage;

/** One color Filter (result is different, regarding the chosen color)
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class OneColour extends AbstractTransformation implements ITransformation {

	private Color mainColour = Color.white;
	private String colour = "White";


	public OneColour(Color mainColour, String colour) {
		this.mainColour = mainColour;
		this.colour = colour;
	}

	@Override
	public void doTransformation(int i, int j, BufferedImage image,
								 BufferedImage img) {
		Color c = new Color(image.getRGB(i, j));
		int dis=(int) Math.pow(c.getRed()- mainColour.getRed(), 2)
		+ (int) Math.pow(c.getBlue()- mainColour.getBlue(), 2)
		+ (int) Math.pow(c.getGreen()- mainColour.getGreen(), 2);
		int nb=(c.getRed()+c.getBlue()+c.getGreen())/3;
		int marge = 30000;
		if(dis< marge){
			img.setRGB(i,j,c.getRGB());
		}
		else {
			img.setRGB(i,j,new Color(nb, nb, nb).getRGB());
		}

	}

	@Override
	public String getIdentification() {
		return "Couleur " + colour;
	}

	@Override
	public boolean isUnique() {
		return true;
	}



}
