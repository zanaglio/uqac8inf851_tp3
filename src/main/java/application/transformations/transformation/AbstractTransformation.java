package application.transformations.transformation;

import application.transformations.ITransformation;

import java.awt.image.BufferedImage;

/** Abstract class to describe a color transformation
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public abstract class AbstractTransformation implements ITransformation {

	/**
	 * Browse the image pixels and apply the transformation on each pixel. The resulting pixel is
	 * placed in a new image
	 *
	 * @param image Image to modify
	 * @return the modified image, which is a copy of the variable image in parameter
	 */
	@Override
	public BufferedImage transform(BufferedImage image){
		int w = image.getWidth();
		int h = image.getHeight();
		BufferedImage img = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB);
		int[] r = new int[w*h];
		image.getRGB(0,0,w,h,r,0,w);
		img.getRaster().setDataElements(0, 0, w, h, r);
		for(int i=0; i<w; i++)
		{
			for(int j=0; j<h; j++)
			{
				doTransformation(i, j, image, img);
			}
		}
		return img;
	}
	
	protected abstract void doTransformation(int i, int j, BufferedImage image, BufferedImage img);

}
