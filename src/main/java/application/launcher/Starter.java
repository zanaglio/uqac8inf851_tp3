package application.launcher;

import application.gui.MainWindow;

import javax.swing.*;

/** Main class of the project which start the program
 * @author Alexis
 * @author Raphael
 * @version 1.2
 */
public class Starter {
	
	public static void main(String[] args){
		try{
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		}
		catch(Exception e){}
		new MainWindow();
	}

}
