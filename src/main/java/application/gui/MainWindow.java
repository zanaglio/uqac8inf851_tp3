package application.gui;

import application.gui.menu.Menu;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/** Main window
 * @author Alexis
 * @author Raphael
 * @version 1.2
 */
public class MainWindow extends JFrame {

	private static final long serialVersionUID = -7973524383994420294L;

	private PanelImage source;
	private PanelImage resultat;
	private final ListTransformations transformations;
	/**
	 * @return the resultat
	 */
	public PanelImage getResultat() {
		return resultat;
	}

	/**
	 * @return the source
	 */
	public PanelImage getSource() {
		return source;
	}

	/**
	 * @return the transformations
	 */
	public ListTransformations getTransformations() {
		return transformations;
	}

	public MainWindow(){

		transformations = new ListTransformations(this);

		//Create TopMenu
		JPanel nord = new JPanel(new BorderLayout());
		nord.add(new Menu(this));
		add(nord, BorderLayout.NORTH);

		//Create ToolBar
		JPanel barreOutils = createToolBar();
		add(barreOutils, BorderLayout.WEST);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		//Create CenterPanel
		JPanel centerPanel = createCenterPanel();
		add(centerPanel, BorderLayout.CENTER);

		//SetUp MainWindow
		setUp(800,400);
	}

	private JPanel createToolBar(){
		JPanel res = new JPanel();
		res.setBorder(BorderFactory.createEtchedBorder());
		res.add(transformations);
		return res;
	}
	private PanelImage createPanelImage(String title, Color color){
		PanelImage panelImage = new PanelImage();
		panelImage.setBorder(BorderFactory.createTitledBorder(title));
		panelImage.setBackground(color);
		return panelImage;
	}

	private GridBagConstraints createGridBagConstraints(){
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.weightx=2;
		constraints.weighty=1;
		constraints.insets = new Insets(10, 10, 10, 10);
		constraints.fill=GridBagConstraints.BOTH;
		constraints.gridy=0;
		return	constraints;
	}

	private JPanel createCenterPanel(){
		JPanel centre = new JPanel();
		centre.setBorder(BorderFactory.createEtchedBorder());
		centre.setLayout(new GridBagLayout());
		//Create Panels
		source = createPanelImage("Image source", Color.red);
		resultat = createPanelImage("Transformation result",Color.blue);
		//Create Constraints
		GridBagConstraints constraints = createGridBagConstraints();
		//Adding Panel Source
		constraints.gridx=0;
		centre.add(source, constraints);
		//Adding Panel Result
		constraints.gridx=1;
		centre.add(resultat, constraints);
		return centre;
	}

	public void setUp(int width, int height){
		setSize(width, height);
		setLocationRelativeTo(null);
		setVisible(true);
		setLayout(new BorderLayout());
	}

	public void setImage(BufferedImage image){
		source.setImage(image);
		resultat.setImage(image);
		transformations.update();
	}

}
