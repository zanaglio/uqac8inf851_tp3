package application.gui;

import application.listeners.rowlistener.ARowListener;
import application.listeners.rowlistener.DeleteListener;
import application.listeners.rowlistener.DownListener;
import application.listeners.rowlistener.UpListener;
import application.transformations.ITransformation;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/** View where the transformations are located
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class ListTransformations extends JPanel {

	private static final long serialVersionUID = -6043015891914443592L;

	private final JTable table;
	private final List<ITransformation> transformations;
	private final MainWindow fp;

	public JTable getTable() {
		return table;
	}
	public List<ITransformation> getTransformations() {
		return transformations;
	}

	public ListTransformations(MainWindow fp){

		this.fp=fp;
		setLayout(new BorderLayout());

		transformations = new ArrayList<>();
		Object[] entetes = new Object[]{"Transformation"};
		Object[][] data = new Object[0][entetes.length];


		table=new JTable(new DefaultTableModel(data, entetes){

			private static final long serialVersionUID = -8975221726042829454L;

			@Override
			public boolean isCellEditable(int iRowIndex, int iColumnIndex){
				return false;
			}
		});

		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setColumnHeaderView(table.getTableHeader());
		add(scrollPane, BorderLayout.CENTER);
		scrollPane.setPreferredSize(new Dimension(150, 150));

		JPanel toolbar = new JPanel();

		JButton up = createButton("up.png",new UpListener(this,table,transformations));
		toolbar.add(up);

		JButton down = createButton("down.png",new DownListener(this,table,transformations));
		toolbar.add(down);

		JButton delete = createButton("erase.png",new DeleteListener(this,table,transformations));
		toolbar.add(delete);

		add(toolbar, BorderLayout.SOUTH);
	}

	private JButton createButton(String path, ARowListener listener){
		JButton button = new JButton();
		button.setIcon(new ImageIcon(getClass().getClassLoader().getResource(path)));
		button.addActionListener(listener);
		return button;
	}

	public void addTransformation(ITransformation transformation){
		boolean ok=true;
		for(ITransformation t : transformations){
			if(t.getIdentification().equals(transformation.getIdentification()) && t.isUnique()){
				ok=false;
			}
		}
		if(ok){
			transformations.add(transformation);
			((DefaultTableModel)(table.getModel())).addRow(new String[]{transformation.getIdentification()});
			update();
		}
		else {
			JOptionPane.showMessageDialog(null, "La transformation existe déjà", "Ajout impossible", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void deleteTransformation(int indice){
		transformations.remove(indice);
		((DefaultTableModel)(table.getModel())).removeRow(indice);
		update();
	}

	public void update(){
		if(fp.getSource().getImage()!=null){
			fp.getResultat().setImage(fp.getSource().getImage());
			fp.getSource().repaint();
			for(ITransformation t : transformations){
				fp.getResultat().setImage(t.transform(fp.getResultat().getImage()));
			}
		}
	}

}
