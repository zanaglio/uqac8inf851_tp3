package application.gui.menu;

import application.gui.MainWindow;
import application.gui.ListTransformations;

import javax.swing.*;

/** Swing Menu
 * @author Alexis
 * @author Raphael
 * @version 1.5
 */
public class Menu extends JMenuBar {
	
	private static final long serialVersionUID = -6776748595659994783L;

	/**
	 * Initialize the menu wrapper for FileMen and SelectMenu
	 * @param  window Application main window
	 */
	public Menu(final MainWindow window){
		final ListTransformations transformations=window.getTransformations();
		
		/* File */
		FileMenu file = new FileMenu(window);
		this.add(file);

		/* Select */
		SelectMenu select = new SelectMenu(transformations);
        this.add(select);
	}

}
