package application.gui.menu;

import application.gui.ListTransformations;
import application.listeners.ListenerTransformation;
import application.transformations.ITransformation;

import javax.swing.*;

/** Swing Transformation menu
 * @author Alexis
 * @author Raphael
 * @version 1.1
 */
public class TransformationMenuItem extends JMenuItem {

    /**
     * Initialize the application transformation menu item
     * @param transformations List of all the transformations to apply
     * @param text Text to set
     * @param transformation Transformation interface
     * @param position Mnemonic position to set
     */
    public TransformationMenuItem(ListTransformations transformations, String text, ITransformation transformation, int position){
        this.setText(text);
        this.setMnemonic(position);
        this.addActionListener(new ListenerTransformation(transformations,transformation));
    }
}
