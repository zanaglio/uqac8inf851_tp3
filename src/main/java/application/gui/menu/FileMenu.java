package application.gui.menu;

import application.gui.MainWindow;
import application.treatment.SaveImageService;
import application.treatment.OpenImageService;

import javax.swing.*;
import java.awt.event.KeyEvent;

/** Swing FileMenu
 * @author Alexis
 * @author Raphael
 * @version 1.1
 */
class FileMenu extends JMenu {

    /**
     * Initialize the application file menu
     * @param  window Application window
     */
    public FileMenu(final MainWindow window){

        this.setText("File");
        this.setMnemonic(KeyEvent.VK_F);

        JMenuItem open = new JMenuItem("Open");
        open.setMnemonic(KeyEvent.VK_N);
        open.addActionListener(e -> OpenImageService.getInstance().loadImage(window));
        this.add(open);

        JMenuItem save = new JMenuItem("Save");
        save.setMnemonic(KeyEvent.VK_E);
        save.addActionListener(e -> SaveImageService.getInstance().save(window.getResultat().getImage()));

        this.add(save);

        this.addSeparator();

        JMenuItem exit = new JMenuItem("Exit");
        exit.setMnemonic(KeyEvent.VK_Q);
        exit.addActionListener(e -> System.exit(0));
        this.add(exit);

    }

}
