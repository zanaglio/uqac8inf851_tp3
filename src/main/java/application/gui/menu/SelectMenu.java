package application.gui.menu;

import application.utils.JdomParser;
import application.gui.ListTransformations;
import org.jdom2.Document;
import org.jdom2.Element;

import javax.swing.*;

/** Swing Select Menu
 * @author Alexis
 * @author Raphael
 * @version 2.0
 */
class SelectMenu extends JMenu{

    /**
     * Initialize the application select menu
     * @param  transformations List of all the transformations available to load
     */
    public SelectMenu(ListTransformations transformations){

        String xmlFile = "src/application/utils/Menu.xml";
        Document document = JdomParser.getSAXParsedDocument(getClass().getClassLoader().getResource("Menu.xml").getPath());
        /**Read Document Content*/

        Element rootNode = document.getRootElement();
        this.setText(rootNode.getName());

        /**Read Menu Content*/
        rootNode.getChildren().forEach(element->JdomParser.readMenuNode(element,this, transformations));

    }
}
