package application.listeners;

import application.gui.ListTransformations;
import application.transformations.ITransformation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/** Listener when a transformation is add throw the TransformationMenuItem
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class ListenerTransformation implements ActionListener {

	private final ListTransformations transformations;
	private final ITransformation transfo;


	public ListenerTransformation(ListTransformations transformations,
			ITransformation transfo) {
		super();
		this.transformations = transformations;
		this.transfo = transfo;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		transformations.addTransformation(transfo);
	}

}
