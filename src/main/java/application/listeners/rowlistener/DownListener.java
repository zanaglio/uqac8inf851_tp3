package application.listeners.rowlistener;

import application.gui.ListTransformations;
import application.transformations.ITransformation;

import javax.swing.*;
import java.util.List;

/** Listener when the user move down a filter
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class DownListener extends AMoveRowListener {

    public DownListener(ListTransformations listTransformations, JTable table, List<ITransformation> transformations) {
        super(listTransformations, table, transformations);
    }

    @Override
    int getCounter(int counter) {
        return counter +1;
    }

    @Override
    boolean isUp(int counter) {
        return counter +1< this.getTable().getRowCount();
    }

    @Override
    public void modify() {
        super.modify();
    }
}
