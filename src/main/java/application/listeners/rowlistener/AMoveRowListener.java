package application.listeners.rowlistener;

import application.gui.ListTransformations;
import application.transformations.ITransformation;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.List;

/** Abstract Listener when the user change the filter's order
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public abstract class AMoveRowListener extends ARowListener {
    AMoveRowListener(ListTransformations listTransformations, JTable table, List<ITransformation> transformations) {
        super(listTransformations, table, transformations);
    }
    abstract int getCounter(int counter);
    abstract boolean isUp(int counter);
    public void modify() {
        int counter = table.getSelectedRow();
        if (isUp(counter)) {
            ITransformation t = transformations.get(counter);
            transformations.set(counter, transformations.get(getCounter(counter)));
            transformations.set(getCounter(counter), t);
            DefaultTableModel model = ((DefaultTableModel) table.getModel());
            model.setValueAt(model.getValueAt(getCounter(counter), 0), counter, 0);
            model.setValueAt(t.getIdentification(), getCounter(counter), 0);
            listTransformations.update();
            table.getSelectionModel().addSelectionInterval(getCounter(counter), getCounter(counter));
        }
    }
}
