package application.listeners.rowlistener;

import application.gui.ListTransformations;
import application.transformations.ITransformation;

import javax.swing.*;
import java.util.List;

/** Listener when the user move up a listener
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class UpListener extends AMoveRowListener {
    public UpListener(ListTransformations listTransformations, JTable table, List<ITransformation> transformations) {
        super(listTransformations, table, transformations);
    }

    @Override
    int getCounter(int counter) {
        return counter -1;
    }

    @Override
    boolean isUp(int counter) {
        return counter >0;
    }

    @Override
    public void modify() {
        super.modify();
    }
}
