package application.listeners.rowlistener;

import application.gui.ListTransformations;
import application.transformations.ITransformation;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/** Abstract listener when the user do something other than moving the rows
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public abstract class ARowListener implements ActionListener {

    final JTable table;
    final List<ITransformation> transformations;
    final ListTransformations listTransformations;
    protected abstract void modify();

    ARowListener(ListTransformations listTransformations, JTable table, List<ITransformation> transformations) {
        this.listTransformations = listTransformations;
        this.table = table;
        this.transformations = transformations;
    }

    JTable getTable() {
        return table;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(table.getSelectedRow()>=0) {
            modify();
        }
    }

}
