package application.listeners.rowlistener;

import application.gui.ListTransformations;
import application.transformations.ITransformation;

import javax.swing.*;
import java.util.List;

/** Listener when the user remove a filter
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class DeleteListener extends ARowListener {
    public DeleteListener(ListTransformations listTransformations, JTable table, List<ITransformation> transformations) {
        super(listTransformations, table, transformations);
    }

    @Override
    public void modify() {
        listTransformations.deleteTransformation(table.getSelectedRow());
    }
}
