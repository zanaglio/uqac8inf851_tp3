package application.treatment;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/** Main service to save an image
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class SaveImageService {
	
	private static final SaveImageService instance = new SaveImageService();
	
	private SaveImageService(){
		super();
	}
	
	public void save(BufferedImage image){
		JFileChooser f = new JFileChooser();
		f.setDialogTitle("Save image");
		f.setFileFilter(new FileFilter() {

			@Override
			public String getDescription() {
				return "Image jpg";
			}

			@Override
			public boolean accept(File f) {
				return f.isDirectory() || f.getName().toLowerCase().endsWith(".jpg");
			}
		});
		if(f.showSaveDialog(null)==0){
			try {
				String path = f.getSelectedFile().getPath();
				if(!path.toLowerCase().endsWith(".jpg")){
					path=path+".jpg";
				}
				String[] words = path.split("\\.");
				ImageIO.write(image, words[words.length-1], new File(path));
			} 
			catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	public static SaveImageService getInstance() {
		return instance;
	}
	
}
