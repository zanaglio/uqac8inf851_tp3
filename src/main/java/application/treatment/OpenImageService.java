package application.treatment;

import application.gui.MainWindow;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/** Main service to open a image
 * @author Alexis
 * @author Raphael
 * @version 1.0
 */
public class OpenImageService {
	
	private static final OpenImageService instance = new OpenImageService();
	
	private OpenImageService(){
		super();
	}

	
	public void loadImage(MainWindow fp){
		JFileChooser f = new JFileChooser();
		f.setDialogTitle("Choose an image");
		f.setFileFilter(new FileFilter() {
			@Override
			public String getDescription() {
				return "Image jpg";
			}
			@Override
			public boolean accept(File f) {
				return f.isDirectory() || f.getName().toLowerCase().endsWith(".jpg") || f.getName().toLowerCase().endsWith(".png");
			}
		});
		if(f.showSaveDialog(null)==0){
			try {
				BufferedImage image = ImageIO.read(f.getSelectedFile());
				fp.setImage(image);
			} 
			catch (IOException e1) {
				e1.printStackTrace();
			}
		}

	}
	
	public static OpenImageService getInstance() {
		return instance;
	}

	
}
