package application.utils;

import application.gui.ListTransformations;
import application.gui.menu.TransformationMenuItem;
import application.transformations.ITransformation;
import application.transformations.filter.*;
import application.transformations.transformation.*;
import application.transformations.transformation.balanced.GreyLevelBalanced;
import application.transformations.transformation.balanced.Sepia;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/** Main Parser to convert a xml file into a filter Menu. This allows more flexibility with the available filters
 * @author Raphael
 * @version 1.0
 */
public class JdomParser {
    public static Document getSAXParsedDocument(final String fileName)
    {
        SAXBuilder builder = new SAXBuilder();
        Document document = null;
        try
        {
            document = builder.build(fileName);
        }
        catch (JDOMException | IOException e)
        {
            e.printStackTrace();
        }
        return document;
    }
    public static void readMenuNode(Element menuNode, JMenu root, ListTransformations transformations)
    {
        if (Objects.equals(menuNode.getName(), "menu")){
            JMenu menu = new JMenu(menuNode.getAttributeValue("name"));
            menuNode.getChildren().forEach(element->readMenuNode(element,menu,transformations));
            root.add(menu);
        }
        else{
            ITransformation instance = findInstance(menuNode);
            TransformationMenuItem menuItem = new TransformationMenuItem(transformations,menuNode.getText(),instance, KeyEvent.VK_M);
            root.add(menuItem);
        }

    }
    private static ITransformation findInstance(Element element){
        String instanceName = element.getAttributeValue("instance");
        ITransformation instance;
        switch (instanceName){
            case "NoiseFilter":
                instance = NoiseFilter.getInstance();
                break;
            case "GaussianFilter":
                instance = GaussianFilter.getInstance();
                break;
            case "MDIFFilter":
                instance = MDIFFilter.getInstance();
                break;
            case "AverageFilter":
                instance = AverageFilter.getInstance();
                break;
            case "PrewittFilter":
                instance = PrewittFilter.getInstance();
                break;
            case "FilterSobel":
                instance = FilterSobel.getInstance();
                break;
            case "GreyLevelBalanced":
                instance = GreyLevelBalanced.getInstance();
                break;
            case "Sepia":
                instance = Sepia.getInstance();
                break;
            case "OneColour":
                Map<String, Color> colours = getMapColours();
                instance = new OneColour(colours.get(element.getText()),element.getText());
                break;
            case "ReverseColour":
                instance = ReverseColour.getInstance();
                break;
            case "GreyLevel":
                instance = GreyLevel.getInstance();
                break;
            case "BlackAndWhite":
                instance = BlackAndWhite.getInstance();
                break;
            case "Rotation":
                instance = Rotation.getInstance();
                break;
            default:
                instance = Rotation.getInstance();
                break;
        }
        return instance;
    }

    private static Map<String,Color> getMapColours() {
        Map<String, Color> couleurs = new HashMap<>();
        couleurs.put("Red", Color.RED);
        couleurs.put("Orange", Color.ORANGE);
        couleurs.put("Yellow", Color.YELLOW);
        couleurs.put("Green", Color.GREEN);
        couleurs.put("Blue", Color.BLUE);
        couleurs.put("Indigo", new Color(47, 0, 127));
        couleurs.put("Purple", new Color(127, 0, 255));
        return couleurs;
    }

}
