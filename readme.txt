Traitement d'application.resources-------------------
Url     : http://codes-sources.commentcamarche.net/source/55156-application-d-imagesAuteur  : cs_Julien39Date    : 27/07/2013
Licence :
=========

Ce document intitulé "Traitement d'application.resources" issu de CommentCaMarche
(codes-sources.commentcamarche.net) est mis à disposition sous les termes de
la licence Creative Commons. Vous pouvez copier, modifier des copies de cette
source, dans les conditions fixées par la licence, tant que cette note
apparaît clairement.

Description :
=============

Ce programme propose les fonctionnalités suivantes :
- Mise en noir et blanc, niveau de gris, sepia
- Ajout de bruit
- Lissage
- Détection des contours
- Pivoter l'image
- Ne conserver qu'une couleur
Conclusion : 

N'hésitez pas à me remonter les bug.
Je n'ai pas fait de commentaires dans le code pour l'instant, je livrerai sans doute une autre version mieux commentée.

================

Version mise à jour par Alexis Schutzger et Raphael Paoli dans le cadre d'un cours de génie logiciel en amélioration qualité.

- Ajout d'un menu Select par fichier XML
- Refactorisation des classes et des méthodes
- Rassemblement sous des packages différents
- Ajout de documentation

================
